<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
    <h2><?=$arResult["NAME"]?></h2>
<?endif;?>
<div class="news-block">
    <div class="picture-block">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<div class="picture">
            <img class="detail_picture" border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"  alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" />
        </div>

	<?endif?>
    </div>
    <div class="news-text">
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<time class="time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></time>
	<?endif;?>


	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
 	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
 	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<!--<?foreach($arResult["FIELDS"] as $code=>$value):?>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			<br />
	<?endforeach;?>-->
    </div>
    <div class="news-nav">
        <?$rs=CIBlockElement::GetList(array("active_from" => "desc"), array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arResult["IBLOCK_ID"]), false, array("nElementID"=>$arResult["ID"], "nPageSize"=>1), array("ID","CODE"));
        while($ar=$rs->GetNext())
        { $page[] = $ar["ID"];
            $code_name[] = $ar["CODE"];
        }?>

        <?if (count($page) == 2 && $arResult["ID"] == $page[0]):?>
            <a href="<?=$arResult["LIST_PAGE_URL"]?><?=$code_name[1]?>/" class="prev-news">Предыдущий партнер</a>

        <?elseif (count($page) == 3):?>
            <a href="<?=$arResult["LIST_PAGE_URL"]?><?=$code_name[0]?>/" class="next-news">Следующий партнер</a>

            <a href="<?=$arResult["LIST_PAGE_URL"]?><?=$code_name[2]?>/" class="prev-news">Предыдущий партнер</a>

        <?elseif (count($page) == 2 && $arResult["ID"] == $page[1]):?>
            <a href="<?=$arResult["LIST_PAGE_URL"]?><?=$code_name[0]?>/" class="next-news">Следующий партнер</a>
        <?endif;?>
    </div>
</div>

