<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="clients-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_DELETE_CONFIRM')));
	?>
	<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="logo">
            <picture class="preview__picture"><!--[if IE 9]><video style="display: none;"><![endif]-->
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
            <?$renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 131), BX_RESIZE_IMAGE_EXACT, false);?>
            <source media="(min-width: 992px)" srcset="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
            <source media="(max-width: 991px)" srcset="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"><!--[if IE 9]></video><![endif]-->
            <img alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" src="<?=$renderImage["src"]?>" style="max-width:131px;" class="responsive-image preview__image">
				<!--<img class="preview_picture" border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" style="float:left" />-->
		<?else:?>
            <?$arItem["PREVIEW_PICTURE"]["SRC"]="/upload/medialibrary/09b/09bf19dd4bccc74c8f08f44b2f5f34dd.gif";
            $arItem["PREVIEW_PICTURE"]["UNSAFE_SRC"]="/upload/medialibrary/09b/09bf19dd4bccc74c8f08f44b2f5f34dd.gif";
            $arItem["PREVIEW_PICTURE"]["SAFE_SRC"]="/upload/medialibrary/09b/09bf19dd4bccc74c8f08f44b2f5f34dd.gif";?>
            <?$renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 187, "height" => 262), BX_RESIZE_IMAGE_EXACT, false);?>
            <source media="(min-width: 992px)" srcset="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
            <source media="(max-width: 991px)" srcset="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"><!--[if IE 9]></video><![endif]-->
            <img alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="responsive-image preview__image">
        <?endif?>
            </picture>
        </div>
        <div class="desc">
            <div class="text-block">
        <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span><?echo $arItem["NAME"]?></span></a></h2>
        <?endif;?>
        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
            <p><?echo $arItem["PREVIEW_TEXT"];?></p>
        <?endif;?>
            </div>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
            <time class="time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></time>
		<?endif?>

		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			</small><br />
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<small>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</small><br />
		<?endforeach;?>
            <div class="more"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('MEWS_DETAIL_LINK')?></a></div>

        </div>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>
